<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ciclistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciclista-index">
                <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta</h3>
                    <P>consultas de ciclistas</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consultadao'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('ORM', ['site/consultaorm'], ['class' => 'btn btn-primary'])?>
                    </p>
                </div>
            </div>
</div>
